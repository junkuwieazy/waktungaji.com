/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.32-MariaDB : Database - new_waktungaji
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`new_waktungaji` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `new_waktungaji`;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2018_09_06_031644_create_usersTb_table',1),('2018_09_08_080423_create_usersUpdateTb_table',2),('2018_09_12_021547_create_captchaTb_table',3);

/*Table structure for table `users_tb` */

DROP TABLE IF EXISTS `users_tb`;

CREATE TABLE `users_tb` (
  `id_user` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `full_name_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `email_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_handphone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_pp_user` text COLLATE utf8_unicode_ci NOT NULL,
  `path_idcard_user` text COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `kota_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `kelurahan_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_verify` enum('yes','not') COLLATE utf8_unicode_ci NOT NULL,
  `signup_with` enum('self','facebook','twitter','google') COLLATE utf8_unicode_ci NOT NULL,
  `status_user` enum('actived','blocked','deteled') COLLATE utf8_unicode_ci NOT NULL,
  `level_user` enum('user','admin') COLLATE utf8_unicode_ci NOT NULL,
  `user_verify` enum('not','wait','yes') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `users_tb_email_user_unique` (`email_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users_tb` */

insert  into `users_tb`(`id_user`,`full_name_user`,`gender`,`email_user`,`no_handphone`,`password_user`,`path_pp_user`,`path_idcard_user`,`provinsi_user`,`kota_user`,`kecamatan_user`,`kelurahan_user`,`email_verify`,`signup_with`,`status_user`,`level_user`,`user_verify`,`remember_token`,`created_at`) values (1,'Juri Pebs','male','juripebrianto@gmail.com','081510193960','5f4dcc3b5aa765d61d8327deb882cf99','photoprofil/9035620180920034017.png','idcard/9035620180920034017.jpg','36','3674','3674030','50080','yes','self','actived','user','wait','776d4e5297a95235dbca8932d1431fd3','2018-09-08 06:29:13'),(3,'Pebri','male','admin@admin.com','081510193961','ef178730d507fedd3860f1da55a0015a','photoprofil/9035620180920034017.png','idcard/9035620180920034017.jpg','36','3674','3674030','50079','yes','self','actived','admin','yes','cc384700ea3f66f22a19bbad8e73058a','2018-09-22 06:27:46');

/*Table structure for table `users_update_tb` */

DROP TABLE IF EXISTS `users_update_tb`;

CREATE TABLE `users_update_tb` (
  `id_update` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `update_type` enum('verifyemail','resetpassword','updateemail','changepassword') COLLATE utf8_unicode_ci NOT NULL,
  `old_email_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `update_value` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `update_token` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status_update` enum('used','unused') COLLATE utf8_unicode_ci NOT NULL,
  `ip_client` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `used_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_update`),
  UNIQUE KEY `users_update_tb_old_email_user_unique` (`old_email_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users_update_tb` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
