<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_tb', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('full_name_user', 200);
            $table->enum('gender', ['male', 'female']);
            $table->string('email_user')->unique();
            $table->string('no_handphone');
            $table->string('password_user');

            $table->text('path_pp_user');
            $table->text('path_idcard_user');

            $table->string('provinsi_user', 200);
            $table->string('kota_user', 200);
            $table->string('kecamatan_user', 200);
            $table->string('kelurahan_user', 200);

            $table->enum('email_verify', ['yes', 'not']);
            $table->enum('signup_with', ['self', 'facebook', 'twitter', 'google']);
            $table->enum('status_user', ['actived', 'blocked', 'deteled']);

            $table->enum('level_user', ['user', 'admin']);
            $table->enum('user_verify', ['not', 'wait', 'yes']);

            $table->rememberToken();
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_tb');
    }
}
