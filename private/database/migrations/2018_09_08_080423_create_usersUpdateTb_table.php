<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersUpdateTbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_update_tb', function (Blueprint $table) {
            $table->bigIncrements('id_update');
            $table->enum('update_type', ['verifyemail', 'resetpassword', 'updateemail', 'changepassword']);
            $table->string('old_email_user')->unique();
            $table->string('update_value', 200);

            $table->string('update_token', 200);
            $table->enum('status_update', ['used', 'unused']);

            $table->string('ip_client', 200);
            $table->dateTime('used_date');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_update_tb');
    }
}
