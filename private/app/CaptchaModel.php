<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaptchaModel extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 'captcha_tb';
    public $timestamps = false;
}
