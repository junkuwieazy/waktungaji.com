<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckUserApi
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = '2907dba206ad9358861eb2f626728ce7';

        // SEGMENT
        $apis = request()->segment(1); // apis
        $substance = request()->segment(2); // wilayah
        $api_key = request()->segment(3); // api key
        $search = request()->segment(4); // provinsi/kotakabupaten/kecamatan/kelurahan
        $search_by_id = request()->segment(5); // kotakabupaten by provinsiId /kecamatan by kotakabupatenId /kelurahan by kecamatanId

        if ($api_key != '' && $apis == 'apis' && $substance == 'wilayah' || $substance == 'captcha') {
            // CHECK DB
            $userDataKey = DB::table('users_tb')->where('remember_token', $api_key)->first();
            if (count($userDataKey) > 0) {
                // API EXIST

                // TO CONTROLLER
                return $next($request);
            } else {
                return response()->json([
                    'error'   => 1,
                    'data'    => 0,
                    'message' => 'Api key not found, please register first',
                ]);
            }
        } else {
            return response()->json([
                'error'   => 2,
                'data'    => 0,
                'message' => 'Wrong url or parameters',
            ]);
        }
    }
}
