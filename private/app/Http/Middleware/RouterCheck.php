<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class RouterCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUrl = Route::getFacadeRoot()->current()->uri();

        // $userDataSession = $request->userdatafromsession;

        if ($request->userdatafromsession->level_user == 'user') {
            // USERS ALLOW ACCESS
            $userAllow = [
                'home', 'profilku', 'akunku',
            ];

            if (in_array($currentUrl, $userAllow)) {
                // echo $currentUrl.' b '; exit();
                return $next($request);
            } else {
                // echo $currentUrl.' a '; exit();
                return redirect()->route('home');
            }
        } else {
            return $next($request);
        }
    }
}
