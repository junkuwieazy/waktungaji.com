<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //lakukan sesuatu terhadap request yang masuk..

        $currentUrl = Route::getFacadeRoot()->current()->uri();

        // print_r(Session::has('emailUser')); exit();

        if ($currentUrl == 'signin' || $currentUrl == 'signup' || $currentUrl == 'lupapassword' || $currentUrl == 'requestpassword') {
            if (Session::has('emailUser')) {
                $email = session('emailUser');

                $userDataEmail = DB::table('users_tb')->where('email_user', $email)->first();
                if (count($userDataEmail) > 0) {
                    return redirect()->route('home');
                } else {
                    Session::flush();

                    return redirect()->route($currentUrl)->with('error_message', 'Masuk untuk mengakses halaman');
                }
            }
        } else {
            if (Session::has('emailUser')) {
                $email = session('emailUser');

                $userDataEmail = DB::select("SELECT  
                usr.id_user,usr.full_name_user,usr.gender,
                usr.email_user,usr.no_handphone,usr.path_pp_user,
                usr.path_idcard_user,usr.email_verify,usr.signup_with,
                usr.status_user,usr.user_verify,usr.level_user,

                usr.provinsi_user,usr.kota_user,usr.kecamatan_user,usr.kelurahan_user,
                usr.remember_token,

                prv.kode_provinsi,prv.nama_provinsi,
                kab.kode_kabkota,kab.nama_kabkota,
                kec.kode_kecamatan,kec.nama_kecamatan,
                kel.id_kelurahan,kel.nama_kelurahan

                FROM new_waktungaji.users_tb usr
                INNER JOIN waktunga_wilayah.provinsi_tb prv ON usr.provinsi_user = prv.kode_provinsi
                INNER JOIN waktunga_wilayah.kabkota_tb kab ON usr.kota_user = kab.kode_kabkota 
                INNER JOIN waktunga_wilayah.kecamatan_tb kec ON usr.kecamatan_user = kec.kode_kecamatan 
                INNER JOIN waktunga_wilayah.kelurahan_tb kel ON usr.kelurahan_user = kel.id_kelurahan
                WHERE usr.email_user = '".$email."' LIMIT 1");

                // $userDataEmail = DB::table('users_tb')->where('email_user',$email)->first();
                if (count($userDataEmail) > 0) {
                    // SEND VALUE TO CONTROLLER
                    // echo "<pre>";
                    // print_r($userDataEmail[0]); exit();
                    $request->merge(['userdatafromsession' => $userDataEmail[0]]);
                } else {
                    return redirect()->route('signin')->with('error_message', 'Masuk untuk mengakses halaman');
                }
            } else {
                return redirect()->route('signin')->with('error_message', 'Masuk untuk mengakses halaman');
            }
        }

        return $next($request);
    }
}
