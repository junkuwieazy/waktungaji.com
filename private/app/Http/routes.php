<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'middleware' => 'before',
    'uses'       => 'FrontController@index',
]);

Route::get('/signin', [
    'as'         => 'signin',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@index',
]);

Route::post('/signin', [
    'as'         => 'signin',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@signin',
]);

Route::get('/signup', [
    'as'         => 'signup',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@create',
]);

Route::post('/signup', [
    'as'         => 'signup',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@storeuser',
]);

Route::get('/signout', [
    'as'   => 'signout',
    'uses' => 'UsersController@destroy',
]);

Route::get('/lupapassword', [
    'as'         => 'lupapassword',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@lupapassword',
]);

Route::post('/requestpassword', [
    'as'         => 'requestpassword',
    'middleware' => ['ceksession'],
    'uses'       => 'UsersController@requestpassword',
]);

Route::get('/for/{for}/{eml}/{tkn}', [
    'as'         => 'for',
    'middleware' => ['cekuser'],
    'uses'       => 'UsersController@usefor',
]);

// GET IMAGES
Route::get('/storages/{foldername}/{filename}', [
    'as' => 'storages',
    // 'middleware' => ['ceksession'],
    'uses' => 'UsersController@storagefolder',
]);

// USERs

Route::get('/home', [
    'as'         => 'home',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'BackController@index',
]);

Route::get('/profilku', [
    'as'         => 'profilku',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@profilku',
]);

Route::post('/profilku', [
    'as'         => 'profilku',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@updateprofil',
]);

Route::get('/akunku', [
    'as'         => 'akunku',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@akunku',
]);

Route::post('/akunku', [
    'as'         => 'akunku',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@updateprofil',
]);

Route::get('/userswait', [
    'as'         => 'userswait',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@userswait',
]);

Route::get('/userswait/{tokenKey}', [
    'as'         => 'userswaitverify',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@userswaitVerify',
]);

Route::get('/usersapprove', [
    'as'         => 'usersapprove',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@usersapprove',
]);

Route::get('/usersapprove/{tokenKey}', [
    'as'         => 'usersapproveverify',
    'middleware' => ['ceksession', 'cekrouter'],
    'uses'       => 'UsersController@usersapproveVerify',
]);

// APIs

Route::get('/apis/wilayah/{apikey}/provinsi', [
    'as'         => 'provinsi',
    'middleware' => ['cekuserapi'],
    'uses'       => 'ApiController@index',
]);

Route::get('/apis/wilayah/{apikey}/{search}/{kode}', [
    'as'         => 'wilayah',
    'middleware' => ['cekuserapi'],
    'uses'       => 'ApiController@search',
]);

Route::get('/apis/captcha/{apikey}', [
    'as'         => 'captcha',
    'middleware' => ['cekuserapi'],
    'uses'       => 'ApiController@captcha',
]);
