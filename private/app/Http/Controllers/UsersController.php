<?php

namespace App\Http\Controllers;

use App\UsersModel;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $input = 'index';
        $dataToView = [
            'dataDari' => $input,
        ];

        // return view('front/landing', compact($dataToView));
        // print_r($dataToView);
        return View::make('pages/back/login')->with($dataToView);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profilku(Request $request)
    {
        // Data To View
        $dataToView = [
            'userDataSession' => $request->userdatafromsession,
        ];
        // View
        return View::make('pages/back/profilku')->with($dataToView);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function akunku(Request $request)
    {
        // Data To View
        $dataToView = [
            'userDataSession' => $request->userdatafromsession,
        ];
        // View
        return View::make('pages/back/akunku')->with($dataToView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateprofil(Request $request)
    {
        // DATA USERS
        $userDataSession = $request->userdatafromsession;

        if ($request->profilButton == 'editEmail') {
            $rules = [
                'update_email'    => 'required|min:15|email',
                'update_password' => 'required|min:6',
            ];

            $messages = [
                'update_email.required'    => 'Kolom :attribute kosong.',
                'update_password.required' => 'Kolom :attribute kosong.',
            ];

            // $validator = Validator::make($request->all(), , $messages);
            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect('akunku')->withErrors($validator)->withInput();
            } else {
                $emailSession = session('emailUser');

                $where = [
                    'email_user'    => $emailSession,
                    'password_user' => md5($request->update_password),
                ];

                $userData = DB::table('users_tb')->where($where)->first();

                if (count($userData) > 0) {
                    $updatType = 'updateemail';
                    $oldEmailUser = $emailSession;
                    $updateValue = $request->email_user;
                    $updatToken = md5(date('YmdHis'));
                    $statusUpdate = 'unused';
                    $ipClient = $request->ip();
                    $createdAt = date('Y-m-d H:i:s');

                    // Send Email
                    $link = url('/').'/for/verifyemail/'.$updatToken;
                    $EmailFild = 'Klik link dibawah untuk verifikasi perubahan email anda <br>';
                    $EmailFild .= "<a href='".$link."'>".$link.'</a> <br>';
                    $EmailFild .= '<b>Terima kasih. '.date('d F Y')." <a href='http://www.waktungaji.com'>WaktuNgaji.com</a>";

                    // EmailTo|EmailSubject|EmailFild
                    $fileData = $updatType.'|Verifikasi Perubahan Email - WaktuNgaji.com|'.$EmailFild;

                    // Create File
                    $destPath = storage_path().'/sendmail/';
                    if (!is_dir($destPath)) {
                        mkdir($destPath, 0777, true);
                    }

                    $createFile = File::put($destPath.$updatToken.'.txt', $fileData);

                    var_dump($createFile);
                    exit();

                    return redirect()->route('signin')->with('success_message', 'Email telah diubah, cek email anda');
                } else {
                    return redirect()->back()->with('error_message_email', 'Password anda salah');
                }
            }
        } elseif ($request->profilButton == 'editPassword') {
            $rules = [
                'old_password'   => 'required|min:6',
                'new_password'   => 'required|min:6',
                'renew_password' => 'required|min:6|same:new_password',
            ];

            $messages = [
                'old_password.required'   => 'Kolom password lama kosong.',
                'new_password.required'   => 'Kolom password baru kosong.',
                'renew_password.required' => 'Kolom ulangi password baru kosong.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect('akunku')->withErrors($validator)->withInput();
            } else {
                $emailSession = session('emailUser');

                $where = [
                    'email_user'    => $emailSession,
                    'password_user' => md5($request->old_password),
                ];

                $userData = DB::table('users_tb')->where($where)->first();

                if (count($userData) > 0) {
                    $updateUserData = DB::table('users_tb')->where($where)->update(['password_user' => md5($request->new_password)]);

                    if ($updateUserData) {
                        Session::flush();

                        return redirect()->route('signin')->with('success_message', 'Password telah diubah, masuk kembali dengan password baru');
                    } else {
                        return redirect()->back()->with('error_message_pass', 'Password baru tidak boleh sama dengan yang lama');
                    }
                } else {
                    return redirect()->back()->with('error_message', 'Password anda salah');
                }
            }
        } elseif ($request->profilButton == 'editBiodata') {
            if ($userDataSession->user_verify != 'wait') {
                $rules = [
                    'fullName'             => 'required|min:6',
                    'noHandphone'          => 'required|min:6',
                    'gender'               => 'required',
                    'wilayahProvinsi'      => 'required',
                    'wilayahKabupatenKota' => 'required',
                    'wilayahKecamatan'     => 'required',
                    'wilayahKelurahan'     => 'required',
                ];

                if ($userDataSession->path_pp_user == '') {
                    $rulesPp = [
                        'photoProfil' => 'required|image|mimes:jpeg,png,jpg,gif',
                    ];
                } else {
                    $rulesPp = [];
                }

                if ($userDataSession->path_idcard_user == '') {
                    $rulesCard = [
                        'ktpUser' => 'required|image|mimes:jpeg,png,jpg,gif',
                    ];
                } else {
                    $rulesCard = [];
                }

                $rules = $rules + $rulesPp + $rulesCard;

                $messages = [
                    'fullName.required'             => 'Kolom nama lengkap kosong.',
                    'noHandphone'                   => 'Kolom nomor hp kosong.',
                    'gender.required'               => 'Pilih jenis kelamin.',
                    'wilayahProvinsi.required'      => 'Pilih provinsi.',
                    'wilayahKabupatenKota.required' => 'Pilih kabupaten / kota.',
                    'wilayahKecamatan.required'     => 'Pilih kecamatan.',
                    'wilayahKelurahan.required'     => 'Pilih kelurahan.',

                    'photoProfil.required' => 'Kolom file foto profil kosong.',
                    'ktpUser.required'     => 'Kolom file id card / ktp kosong.',
                ];

                $validator = Validator::make($request->all(), $rules, $messages);

                if ($validator->fails()) {
                    return redirect('profilku')->withErrors($validator)->withInput();
                } else {
                    $emailSession = session('emailUser');

                    $where = [
                        'email_user' => $emailSession,
                    ];

                    $userData = DB::table('users_tb')->where($where)->first();

                    if (count($userData) > 0) {
                        $randString = rand(10000, 99999).date('YmdHis');
                        // IMAGE

                        // PP
                        if (count($rulesPp) > 0) {
                            $imagePhotoProfil = $request->file('photoProfil');
                            $imagePhotoProfilName = $randString.'.'.$imagePhotoProfil->getClientOriginalExtension();
                            $imagePhotoProfilPath = storage_path().'/photoprofil/';
                            $createImagePhotoProfil = Image::make($imagePhotoProfil->getRealPath());
                            $createImagePhotoProfil->resize(500, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePhotoProfilPath.'/'.$imagePhotoProfilName);

                            $updateSetPp = [
                                'path_pp_user' => '/photoprofil/'.$imagePhotoProfilName,
                            ];
                        } else {
                            $updateSetPp = [];
                        }

                        // ORIGINAL WITHOUT RESIZE
                        // $destinationPath = public_path('/images');
                        // $image->move($destinationPath, $input['imagename']);
                        // $this->postImage->add($input);

                        // PP
                        if (count($rulesPp) > 0) {
                            $imageIdCard = $request->file('ktpUser');
                            $imageIdCardName = $randString.'.'.$imageIdCard->getClientOriginalExtension();
                            $imageIdCardPath = storage_path().'/idcard/';
                            $createImageIdCard = Image::make($imageIdCard->getRealPath());
                            $createImageIdCard->resize(500, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imageIdCardPath.'/'.$imageIdCardName);

                            $updateSetCard = [
                                'path_idcard_user' => '/idcard/'.$imageIdCardName,
                            ];
                        } else {
                            $updateSetCard = [];
                        }

                        if (
                            $userDataSession->no_handphone == '' ||
                            $userDataSession->path_pp_user == '' ||
                            $userDataSession->path_idcard_user == ''
                        ) {
                            $uVerify = 'not';
                        } else {
                            $uVerify = 'wait';
                        }

                        // UPDATE
                        $updateSet = [
                            'gender'         => $request->gender,
                            'no_handphone'   => $request->noHandphone,
                            'provinsi_user'  => $request->wilayahProvinsi,
                            'kota_user'      => $request->wilayahKabupatenKota,
                            'kecamatan_user' => $request->wilayahKecamatan,
                            'kelurahan_user' => $request->wilayahKelurahan,
                            'user_verify'    => $uVerify,
                        ];

                        $updateSet = $updateSet + $updateSetPp + $updateSetCard;
                        DB::connection()->enableQueryLog();
                        $updateUserData = DB::table('users_tb')->where($where)->update($updateSet);
                        if ($updateUserData) {
                            return redirect()->back()->with('success_message', 'Data anda sudah diperbaharui, verifikasi akan dilakukan Admin');
                        } else {
                            DB::connection()->enableQueryLog();
                            // QUERY YANG MAU DILIAT
                            $query = DB::getQueryLog();
                            echo '<pre>';
                            print_r($query);
                        }
                    } else {
                        return redirect()->route('signin')->with('error_message', 'Masuk untuk mengakses halaman');
                    }
                }
            } else {
                return redirect()->back()->with('error_message', 'Menunggu moderasi admin');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $input = 'create';
        $dataToView = [
            'dataDari' => $input,
        ];

        // return view('front/landing', compact($dataToView));
        // print_r($dataToView);
        return View::make('pages/back/register')->with($dataToView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeuser(Request $request)
    {
        $rules = [
            'namaLengkap'        => 'required|min:4',
            'alamatEmail'        => 'required|min:15|email|unique:users_tb,email_user',
            'password'           => 'required|min:6',
            'konfirmasiPassword' => 'required|min:6|same:password',
            'terms'              => 'required',
        ];

        $messages = [
            'namaLengkap.required'        => 'Kolom attribute kosong.',
            'alamatEmail.required'        => 'Kolom attribute kosong.',
            'password.required'           => 'Kolom attribute kosong.',
            'konfirmasiPassword.required' => 'Kolom attribute kosong.',
            'terms.required'              => 'Kolom attribute kosong.',
        ];

        // $validator = Validator::make($request->all(), , $messages);
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('signup')->withErrors($validator)->withInput();
        } else {
            $userData = new UsersModel();
            $userData->full_name_user = $request->namaLengkap;
            $userData->email_user = $request->alamatEmail;
            $userData->password_user = md5($request->password);

            $userData->email_verify = 'not';
            $userData->signup_with = 'self';
            $userData->status_user = 'actived';

            $newToken = md5(date('YmdHis'));
            $userData->remember_token = $newToken;
            $userData->created_at = date('Y-m-d H:i:s');

            // SEND EMAIL ACTIVATION
            $activation = '/for/verifyemail/'.$newToken; // BASE URL url('/').
            $sendMail = true;

            if ($sendMail) {
                if ($userData->save()) {
                    return redirect()->back()->with('success_message', 'Periksa email anda untuk verifikasi')->withInput();
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function signin(Request $request)
    {
        $rules = [
            'alamatEmail' => 'required|min:15|email',
            'password'    => 'required|min:6',
        ];

        $messages = [
            'alamatEmail.required' => 'Kolom attribute kosong.',
            'password.required'    => 'Kolom attribute kosong.',
        ];

        // $validator = Validator::make($request->all(), , $messages);
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('signin')->withErrors($validator)->withInput();
        } else {
            $where = [
                'email_user'    => $request->alamatEmail,
                'password_user' => md5($request->password),
            ];

            $userData = DB::table('users_tb')->where($where)->first();

            if (count($userData) > 0) {
                $request->session()->put('emailUser', $userData->email_user);

                return redirect()->route('home');
            } else {
                $userDataEmail = DB::table('users_tb')->where('email_user', $request->alamatEmail)->first();
                if (count($userDataEmail) > 0) {
                    return redirect()->back()->with('error_message', 'Email atau Password anda tidak cocok')->withInput();
                } else {
                    return redirect()->back()->with('error_message', 'Anda belum terdaftar')->withInput();
                }
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lupapassword()
    {
        //

        $input = 'index';
        $dataToView = [
            'dataDari' => $input,
        ];

        // return view('front/landing', compact($dataToView));
        // print_r($dataToView);
        return View::make('pages/back/lupapassword')->with($dataToView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function requestpassword(Request $request)
    {
        $rules = [
            'alamatEmail' => 'required|min:15|email',
        ];

        $messages = [
            'alamatEmail.required' => 'Kolom attribute kosong.',
        ];

        // $validator = Validator::make($request->all(), , $messages);
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('lupapassword')->withErrors($validator)->withInput();
        } else {
            $userDataEmail = DB::table('users_tb')->where('email_user', $request->alamatEmail)->first();
            if (count($userDataEmail) > 0) {

                // SEND EMAIL ACTIVATION
                $newToken = md5(date('YmdHis'));
                $activation = '/for/resetpassword/'.$newToken; // BASE URL url('/').
                $sendMail = true;
                if ($sendMail) {
                    if ($userData->save()) {
                        return redirect()->back()->with('message', 'Reset password dikirim ke email anda');
                    }
                }
            } else {
                return redirect()->back()->with('message', 'Email belum terdaftar');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function usefor()
    {
        //
        $url = request()->segment(1); // verify
        $for = request()->segment(2); // email or password
        $email = request()->segment(3);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function destroy()
    {
        //
        Session::flush();

        return redirect()->route('signin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function userswait(Request $request)
    {
        // RAW
        $userWaitStatus = DB::select("SELECT  
        usr.id_user,usr.full_name_user,usr.gender,
        usr.email_user,usr.no_handphone,usr.path_pp_user,
        usr.path_idcard_user,usr.email_verify,usr.signup_with,
        usr.status_user,usr.user_verify,

        usr.provinsi_user,usr.kota_user,usr.kecamatan_user,usr.kelurahan_user,
        usr.remember_token,

        prv.kode_provinsi,prv.nama_provinsi,
        kab.kode_kabkota,kab.nama_kabkota,
        kec.kode_kecamatan,kec.nama_kecamatan,
        kel.id_kelurahan,kel.nama_kelurahan

        FROM new_waktungaji.users_tb usr
        INNER JOIN waktunga_wilayah.provinsi_tb prv ON usr.provinsi_user = prv.kode_provinsi
        INNER JOIN waktunga_wilayah.kabkota_tb kab ON usr.kota_user = kab.kode_kabkota 
        INNER JOIN waktunga_wilayah.kecamatan_tb kec ON usr.kecamatan_user = kec.kode_kecamatan 
        INNER JOIN waktunga_wilayah.kelurahan_tb kel ON usr.kelurahan_user = kel.id_kelurahan
        WHERE usr.level_user = 'user' AND usr.user_verify = 'wait' AND usr.email_verify = 'yes'");

        // PARSE JSON
        $dataLoad = json_encode($userWaitStatus);

        $dataToView = [
            'userDataSession'    => $request->userdatafromsession,
            'userWaitStatusData' => $dataLoad,
        ];

        // return view('front/landing', compact($dataToView));
        // print_r($dataToView);
        return View::make('pages/back/userswait')->with($dataToView);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function userswaitVerify($tokenKey)
    {
        // RAW
        $userDataToken = DB::table('users_tb')->where('remember_token', $tokenKey)->first();

        if (count($userDataToken) > 0) {
            $where = [
                'remember_token' => $tokenKey,
            ];

            $updateSet = [
                'user_verify' => 'yes',
            ];

            $updateUserData = DB::table('users_tb')->where($where)->update($updateSet);
            if ($updateUserData) {
                return redirect()->back()->with('success_message', 'User sudah diverifikas');
            } else {
                return redirect()->back()->with('error_message', 'Gagal query');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function usersapprove(Request $request)
    {
        // RAW
        $userWaitStatus = DB::select("SELECT  
        usr.id_user,usr.full_name_user,usr.gender,
        usr.email_user,usr.no_handphone,usr.path_pp_user,
        usr.path_idcard_user,usr.email_verify,usr.signup_with,
        usr.status_user,usr.user_verify,

        usr.provinsi_user,usr.kota_user,usr.kecamatan_user,usr.kelurahan_user,
        usr.remember_token,

        prv.kode_provinsi,prv.nama_provinsi,
        kab.kode_kabkota,kab.nama_kabkota,
        kec.kode_kecamatan,kec.nama_kecamatan,
        kel.id_kelurahan,kel.nama_kelurahan

        FROM new_waktungaji.users_tb usr
        INNER JOIN waktunga_wilayah.provinsi_tb prv ON usr.provinsi_user = prv.kode_provinsi
        INNER JOIN waktunga_wilayah.kabkota_tb kab ON usr.kota_user = kab.kode_kabkota 
        INNER JOIN waktunga_wilayah.kecamatan_tb kec ON usr.kecamatan_user = kec.kode_kecamatan 
        INNER JOIN waktunga_wilayah.kelurahan_tb kel ON usr.kelurahan_user = kel.id_kelurahan
        WHERE usr.level_user = 'user' AND usr.user_verify = 'yes' AND usr.email_verify = 'yes'");

        // PARSE JSON
        $dataLoad = json_encode($userWaitStatus);

        $dataToView = [
            'userDataSession'    => $request->userdatafromsession,
            'userWaitStatusData' => $dataLoad,
        ];

        // return view('front/landing', compact($dataToView));
        // print_r($dataToView);
        return View::make('pages/back/usersapprove')->with($dataToView);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function usersapproveVerify($tokenKey)
    {
        // RAW
        $userDataToken = DB::table('users_tb')->where('remember_token', $tokenKey)->first();

        if (count($userDataToken) > 0) {
            $where = [
                'remember_token' => $tokenKey,
            ];

            if ($userDataToken->status_user == 'actived') {
                $stat = 'blocked';
            } elseif ($userDataToken->status_user == 'blocked') {
                $stat = 'actived';
            }

            $updateSet = [
                'status_user' => $stat,
            ];

            $updateUserData = DB::table('users_tb')->where($where)->update($updateSet);
            if ($updateUserData) {
                return redirect()->back()->with('success_message', 'User sudah diverifikas');
            } else {
                return redirect()->back()->with('error_message', 'Gagal query');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    public function storagefolder()
    {
        //
        $storages = request()->segment(1); // Storage Router
        $foldername = request()->segment(2); // Folder Name
        $filename = request()->segment(3); // File Name

        if ($storages == 'storages') {
            $getFile = storage_path().'/'.$foldername.'/'.$filename;

            if (File::exists($getFile)) {
                $filename = basename($getFile);
                $file_extension = strtolower(substr(strrchr($filename, '.'), 1));
                switch ($file_extension) {
                    case 'gif': $ctype = 'image/gif'; break;
                    case 'png': $ctype = 'image/png'; break;
                    case 'jpeg':
                    case 'jpg': $ctype = 'image/jpeg'; break;
                    default:
                }

                header('Content-type: '.$ctype);
                $showFile = file_get_contents($getFile);
                echo $showFile;
            }
        }
    }
}
