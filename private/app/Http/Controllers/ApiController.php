<?php

namespace App\Http\Controllers;

use App\CaptchaModel;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $provinsi = DB::connection('mysql2')->table('provinsi_tb')->get();

        foreach ($provinsi as $key => $value) {
            // code...
            $publicData[$key]['kode_provinsi'] = $value->kode_provinsi;
            $publicData[$key]['nama_provinsi'] = $value->nama_provinsi;
        }

        return response()->json($publicData);
    }

    public function search($apikey, $search, $kode)
    {
        //
        if ($search == 'kabupatenkota') {
            $tableIs = 'kabkota_tb';
            $whereIs = 'kode_provinsi';
            // ARR
            $arrKode = 'kode_kabkota';
            $arrName = 'nama_kabupatenkota';
        } elseif ($search == 'kecamatan') {
            $tableIs = 'kecamatan_tb';
            $whereIs = 'kode_kabkota';
            // ARR
            $arrKode = 'kode_kecamatan';
            $arrName = 'nama_kecamatan';
        } elseif ($search == 'kelurahan') {
            $tableIs = 'kelurahan_tb';
            $whereIs = 'kode_kecamatan';
            // ARR
            $arrKode = 'kode_kelurahan';
            $arrName = 'nama_kelurahan';
        } else {
            return response()->json([
                'error'   => 1,
                'data'    => 0,
                'message' => 'Sub wilayah tidak ditemukan, sub wilayah hanya kabupatenkota, kecamatan, kelurahan',
            ]);

            exit();
        }

        $searchFor = DB::connection('mysql2')->table($tableIs)->where($whereIs, $kode)->get();

        $publicData = [];

        foreach ($searchFor as $key => $value) {
            // code...
            if ($search == 'kabupatenkota') {
                $publicData[$key][$arrKode] = $value->kode_kabkota;
                $publicData[$key][$arrName] = $value->nama_kabkota;
            } elseif ($search == 'kecamatan') {
                $publicData[$key][$arrKode] = $value->kode_kecamatan;
                $publicData[$key][$arrName] = $value->nama_kecamatan;
            } elseif ($search == 'kelurahan') {
                $publicData[$key][$arrKode] = $value->id_kelurahan;
                $publicData[$key][$arrName] = $value->nama_kelurahan;
            }
        }
        // echo "<pre>";
        // print_r($search);
        return response()->json($publicData);
    }

    public function captcha()
    {
        // code...
        do {
            // GET CAPTCHA
            $captchaString = '';
            for ($i = 0; $i < 6; $i++) {
                $x = mt_rand(0, 2);
                switch ($x) {
                    case 0: $captchaString .= chr(mt_rand(97, 122)); break;
                    case 1: $captchaString .= chr(mt_rand(65, 90)); break;
                    case 2: $captchaString .= chr(mt_rand(48, 57)); break;
                }
            }
            //CHECK CODE
            $searchCaptcha = DB::connection('mysql2')->table('captcha_tb')->where('captcha_code', $captchaString)->get();
            if (empty($searchCaptcha)) {
                $captchaData = new CaptchaModel();
                $captchaData->captcha_code = $captchaString;
                $captchaData->created_at = date('Y-m-d H:i:s');
                if ($captchaData->save()) {
                    return response()->json([
                        'error'   => 0,
                        'data'    => $captchaString,
                        'message' => 'Code captcha anda '.$captchaString,
                    ]);

                    exit();
                }
            }
        } while (!empty($searchCaptcha));
    }
}
