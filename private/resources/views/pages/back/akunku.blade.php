@extends('templates.back')

@section('backtitle', 'Akun Ku')

@section('extra-style')

@endsection

@section('content')

<div id="page-wrapper">
    <div style="margin-top: 70px" class="row">

    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-envelope fa-fw"></i> <b>Email</b> <small>Email aktif anda, jika ingin diubah silahkan masukan email dan password anda saat ini</small>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(session()->has('success_message_email'))
                        <code style="color: blue"> 
                            {{ session()->get('success_message_email') }}
                        </code>
                    @endif
                    @if(session()->has('error_message_email'))
                        <code style="color: red">
                            {{ session()->get('error_message_email') }}
                        </code>
                    @endif

                    <form method="POST" action="{{ route('profilku') }}">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('update_email') ? ' error' : '' }}">
                                    <label for="exampleInputEmail1">Alamat Email</label>
                                    @if ($errors->has('update_email'))
                                        <label class="error" for="update_email">{{ $errors->first('update_email') }}</label>
                                    @endif
                                    <div class="input-group">
                                        <input required="" name="update_email" class="form-control" type="text" value="{{$userDataSession->email_user}}" placeholder="Ketik email anda">
                                        @if ($userDataSession->email_verify == 'not')
                                        <div class="input-group-addon">Email belum diverifikasi</div>
                                        @else
                                        <div class="input-group-addon">Email terverifikasi</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('update_password') ? ' error' : '' }}">
                                    <label for="exampleInputPassword1">Password</label>
                                    @if ($errors->has('update_email'))
                                        <label id="name-error" class="error" for="update_email">{{ $errors->first('update_email') }}</label>
                                    @endif
                                    <input required="" name="update_password" class="form-control" type="password" value="{{old('update_password')}}" placeholder="Ketik password lama">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button name="profilButton" value="editEmail" type="submit" type="button" class="btn btn-success">Update Email</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>


        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-eye-slash fa-fw"></i> <b>Password</b> <small>Jika anda ingin mengubah password, anda dapat mengubah password pada kolom dibawah ini</small>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(session()->has('success_message_pass'))
                        <code style="color: blue"> 
                            {{ session()->get('success_message_pass') }}
                        </code>
                    @endif
                    @if(session()->has('error_message_pass'))
                        <code style="color: red">
                            {{ session()->get('error_message_pass') }}
                        </code>
                    @endif

                    <form method="POST" action="{{ route('akunku') }}">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('old_password') ? ' error' : '' }}">
                                    <label class="form-label">Password Lama</label>
                                    @if ($errors->has('old_password'))
                                        <label class="error" for="old_password">{{ $errors->first('old_password') }}</label>
                                    @endif
                                    <input required="" name="old_password" class="form-control" type="password" value="{{old('old_password')}}" placeholder="Ketik password lama">
                                </div>
                                <div class="form-group {{ $errors->has('new_password') ? ' error' : '' }}">
                                    <label class="form-label">Password Baru</label>
                                    @if ($errors->has('new_password'))
                                        <label class="error" for="new_password">{{ $errors->first('new_password') }}</label>
                                    @endif
                                    <input required="" name="new_password" class="form-control" type="password" value="{{old('new_password')}}" placeholder="Ketik password baru">
                                </div>
                                <div class="form-group {{ $errors->has('renew_password') ? ' error' : '' }}">
                                    <label class="form-label">Ulangi Password Baru</label>
                                    @if ($errors->has('renew_password'))
                                        <label id="name-error" class="error" for="renew_password">{{ $errors->first('renew_password') }}</label>
                                    @endif
                                     <input required="" name="renew_password" class="form-control" type="password" value="{{old('renew_password')}}" placeholder="Ketik ulang password lama">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button name="profilButton" value="editPassword" type="submit" type="button" class="btn btn-success">Update Password</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>


@stop


@section('extra-script')

@endsection