<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Akses Masuk | WaktuNgaji.com</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('assets/startbootstrap/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ URL::asset('assets/startbootstrap/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('assets/startbootstrap/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('assets/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet') }}" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ URL::asset('assets/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('assets/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('assets/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('assets/favicon/favicon-16x16.pn') }}g">
    <link rel="manifest" href="{{ URL::asset('assets/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ URL::asset('assets/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

</head>

<body style="background-color: #32a5a1">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center;">Masuk Akses WaktuNgaji.com</h3>
                    </div>

                    @if(session()->has('success_message'))
                        <div class="alert alert-success" style="text-align: center;">
                            {{ session()->get('success_message') }}
                        </div>
                    @endif
                    @if(session()->has('error_message'))
                        <div class="alert alert-danger" style="text-align: center;">
                            {{ session()->get('error_message') }}
                        </div>
                    @endif

                    <div class="panel-body">
                        <form role="form" method="POST" action="{{ route('signin') }}">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group {{ $errors->has('alamatEmail') ? ' error' : '' }}">
                                    <input type="text" class="form-control" name="alamatEmail" value="{{old('alamatEmail')}}" placeholder="Alamat Email" required autofocus>
                                </div>
                                @if ($errors->has('alamatEmail'))
                                    <label id="name-error" class="error" for="alamatEmail">{{ $errors->first('alamatEmail') }}</label>
                                @endif

                                <div class="form-group {{ $errors->has('password') ? ' error' : '' }}">
                                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('password') }}</label>
                                @endif
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} value="Remember Me">Ingatkan saya
                                    </label>
                                </div>

                                <button class="btn btn-lg btn-success btn-block" type="submit">Masuk</button>

                                <div class="row" style="margin-top: 20px">
                                    <div class="col-md-6">
                                        <a href="{{route('signup')}}">Belum punya akun ?</a>
                                    </div>
                                    <div class="col-md-6 align-right">
                                        <a href="{{route('lupapassword')}}">Lupa Password ?</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/dist/js/sb-admin-2.js') }}"></script>

</body>

</html>
