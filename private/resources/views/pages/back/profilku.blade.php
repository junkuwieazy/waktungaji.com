@extends('templates.back')

@section('backtitle', 'Profil Ku')

@section('extra-style')

@endsection

@section('content')

<div id="page-wrapper">
    <div style="margin-top: 70px" class="row">

    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> <b>Biodata</b> <small>Upload Foto profile dan KTP anda agar dapat diverifikasi</small>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(session()->has('success_message'))
                        <code style="color: blue"> 
                            {{ session()->get('success_message') }}
                        </code>
                    @endif
                    @if(session()->has('error_message'))
                        <code style="color: red">
                            {{ session()->get('error_message') }}
                        </code>
                    @endif

                    <form method="POST" action="{{ route('profilku') }}" enctype="multipart/form-data" id="formBiodata">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <!-- <div class="row"> -->

                                <div class="col-md-4">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('fullName') ? ' error' : '' }}">
                                            <label class="form-label">Nama Lengkap</label>
                                            @if ($errors->has('fullName'))
                                                <label id="name-error" class="error" for="fullName">{{ $errors->first('fullName') }}</label>
                                            @endif
                                            <input required="" name="fullName" id="email_address" class="form-control" type="text" value="{{$userDataSession->full_name_user}}">
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div class="col-md-4">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('noHandphone') ? ' error' : '' }}">
                                            <label class="form-label">No. Handphone</label>
                                            @if ($errors->has('noHandphone'))
                                                <label id="name-error" class="error" for="noHandphone">{{ $errors->first('noHandphone') }}</label>
                                            @endif
                                            <input required="" name="noHandphone" id="email_address" class="form-control" type="text" value="{{$userDataSession->no_handphone}}">
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div class="col-md-4">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('gender') ? ' error' : '' }}">
                                            <label class="form-label">Jenis Kelamin</label>
                                            @if ($errors->has('gender'))
                                                <label id="name-error" class="error" for="gender">{{ $errors->first('gender') }}</label>
                                            @endif
                                            <select required="" name="gender" class="form-control">
                                                <option value="0">-Pilih Gender-</option>
                                                <option {{ $userDataSession->gender == 'male' ? ' selected' : '' }} value="male">Laki - Laki</option>
                                                <option {{ $userDataSession->gender == 'female' ? ' selected' : '' }}  value="female">Perempuan</option>
                                            </select>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            <!-- </div> -->
                        </div>

                        <div class="col-md-12">
                            <!-- <div class="row"> -->

                                <div class="col-md-3">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('wilayahProvinsi') ? ' error' : '' }}">
                                            <label class="form-label">Provinsi</label>
                                            @if ($errors->has('wilayahProvinsi'))
                                                <label class="error" for="wilayahProvinsi">{{ $errors->first('wilayahProvinsi') }}</label>
                                            @endif
                                            <select required="" id="wilayahProvinsi" name="wilayahProvinsi" class="form-control"></select>
                                        </div>
                                    <!-- </div> -->
                                </div>

                                <div class="col-md-3">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('wilayahKabupatenKota') ? ' error' : '' }}">
                                            <label class="form-label">Kabupaten / Kota</label>
                                            @if ($errors->has('wilayahKabupatenKota'))
                                                <label class="error" for="wilayahKabupatenKota">{{ $errors->first('wilayahKabupatenKota') }}</label>
                                            @endif
                                            <select required="" id="wilayahKabupatenKota" name="wilayahKabupatenKota" class="form-control"></select>
                                        </div>
                                    <!-- </div> -->
                                </div>

                                <div class="col-md-3">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('wilayahKecamatan') ? ' error' : '' }}">
                                            <label class="form-label">Kecamatan</label>
                                            @if ($errors->has('wilayahKecamatan'))
                                                <label class="error" for="wilayahKecamatan">{{ $errors->first('wilayahKecamatan') }}</label>
                                            @endif
                                            <select required="" id="wilayahKecamatan" name="wilayahKecamatan" class="form-control"></select>
                                        </div>
                                    <!-- </div> -->
                                </div>

                                <div class="col-md-3">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('wilayahKelurahan') ? ' error' : '' }}">
                                            <label class="form-label">Kelurahan</label>
                                            @if ($errors->has('wilayahKelurahan'))
                                                <label class="error" for="wilayahKelurahan">{{ $errors->first('wilayahKelurahan') }}</label>
                                            @endif
                                            <select required="" id="wilayahKelurahan" name="wilayahKelurahan" class="form-control"></select>
                                        </div>
                                    <!-- </div> -->
                                </div>

                            <!-- </div> -->
                        </div>

                        <div class="col-md-12">
                            <!-- <div class="row"> -->

                                <div class="col-md-6">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('photoProfil') ? ' error' : '' }}">
                                            <label class="form-label">Foto Profil</label>
                                            <img style="height: 200px" class="img img-responsive" src="{{ URL::to('/').'/storages/'.$userDataSession->path_pp_user }}">
                                            @if ($errors->has('photoProfil'))
                                                <label class="error" for="photoProfil">{{ $errors->first('photoProfil') }}</label>
                                            @endif
                                            <input name="photoProfil" id="password" class="form-control" type="file" value="{{old('photoProfil')}}">
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div class="col-md-6">
                                    <!-- <div class="row"> -->
                                        <div class="form-group {{ $errors->has('ktpUser') ? ' error' : '' }}">
                                            <label class="form-label">ID Card / KTP</label>
                                            <img style="height: 200px" class="img img-responsive" src="{{ URL::to('/').'/storages/'.$userDataSession->path_idcard_user }}">
                                            @if ($errors->has('ktpUser'))
                                                <label id="name-error" class="error" for="ktpUser">{{ $errors->first('ktpUser') }}</label>
                                            @endif
                                            <input name="ktpUser" id="password" class="form-control" type="file" value="{{old('ktpUser')}}">
                                        </div>
                                    <!-- </div> -->
                                </div>

                            <!-- </div> -->
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button name="profilButton" value="editBiodata" type="submit" type="button" class="btn btn-success">Update Biodata</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>

<div id="warningUserWait" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <h2 style="text-align: center;">Data anda sedang menunggu verifikasi oleh Admin</h2>
        </div>
    </div>
</div>

<div id="warningEmailVerify" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <h2 style="text-align: center;">Anda belum melakukan verifikasi email</h2>
        </div>
    </div>
</div>  


@stop


@section('extra-script')
<script type="text/javascript">
    $(document).ready(function () {
        // DISABLED
        @if($userDataSession->user_verify == 'wait')
            $('#formBiodata').find('input, textarea, button, select').attr('disabled','disabled');

            $('#warningUserWait').modal('show');
        @endif

        @if($userDataSession->email_verify == 'not')
            $('#formBiodata').find('input, textarea, button, select').attr('disabled','disabled');

            $('#warningEmailVerify').modal('show');
        @endif

        getProvinsi()

        @if($userDataSession->provinsi_user  != '')
            kabupatenkotaBy({!! $userDataSession->provinsi_user !!})
        @endif

        @if($userDataSession->kota_user != '')
            getKecamatanBy({!! $userDataSession->kota_user !!})            
        @endif

        @if($userDataSession->kecamatan_user != '')
            getKelurahanBy({!! $userDataSession->kecamatan_user !!})
        @endif
    });

    $("#wilayahProvinsi").on("change", function (e) {
        var provinsi = $("#wilayahProvinsi").val();
        kabupatenkotaBy(provinsi)
    });

    $("#wilayahKabupatenKota").on("change", function (e) {
        var kabupatenkota = $("#wilayahKabupatenKota").val();
        getKecamatanBy(kabupatenkota)
    });

    $("#wilayahKecamatan").on("change", function (e) {
        var kecamatan = $("#wilayahKecamatan").val();
        getKelurahanBy(kecamatan)
    });

    // FUNCTION
    function getProvinsi() {
        // body...
        $.ajax({
            type: 'GET',
            url: "{{ URL::to('/') }}/apis/wilayah/{{ $userDataSession->remember_token }}/provinsi",
            success: function (data) {

                @if($userDataSession->provinsi_user == '')
                    $('#wilayahProvinsi').append('<option disabled selected value="0">-Pilih Provinsi-</option>');
                    var prv = 0;
                @else
                    var prv = {!! $userDataSession->provinsi_user !!}  
                @endif
                
                $.each(data, function (index, value) {
                    // APPEND OR INSERT DATA TO SELECT ELEMENT.
                    if(value.kode_provinsi === prv) {
                        $('#wilayahProvinsi').append('<option selected value="' + value.kode_provinsi + '">' + value.nama_provinsi + '</option>');
                    } else {
                        $('#wilayahProvinsi').append('<option value="' + value.kode_provinsi + '">' + value.nama_provinsi + '</option>');
                    }
                });
            }
        });

        $('#wilayahKabupatenKota').append('<option disabled selected value="0">-Pilih Provinsi-</option>');
        $("#wilayahKecamatan").append('<option disabled selected value="0">-Pilih Provinsi-</option>');
        $("#wilayahKelurahan").append('<option disabled selected value="0">-Pilih Provinsi-</option>');
    }


    function kabupatenkotaBy(provinsiId) {
        // body...
        $.ajax({
            type: 'GET',
            url: "{{ URL::to('/') }}/apis/wilayah/{{ $userDataSession->remember_token }}/kabupatenkota/"+provinsiId,
            success: function (data) {

                @if($userDataSession->kota_user == '')
                    $("#wilayahKabupatenKota").append('<option disabled selected value="0">-Pilih Kabupaten / Kota-</option>');
                    var kab = 0;
                @else
                    var kab = {!! $userDataSession->kota_user !!}
                @endif

                $.each(data, function (index, value) {
                    // APPEND OR INSERT DATA TO SELECT ELEMENT.
                    if(value.kode_kabkota === kab) {
                        $('#wilayahKabupatenKota').append('<option selected value="' + value.kode_kabkota + '">' + value.nama_kabupatenkota + '</option>');
                    } else {
                        $('#wilayahKabupatenKota').append('<option value="' + value.kode_kabkota + '">' + value.nama_kabupatenkota + '</option>');
                    }
                });
            }
        });

        $('#wilayahKabupatenKota').find('option').remove()
        $("#wilayahKabupatenKota").append('<option disabled selected value="0">-Pilih Kabupaten / Kota-</option>');

        $('#wilayahKecamatan').find('option').remove()
        $("#wilayahKecamatan").append('<option disabled selected value="0">-Pilih Kabupaten / Kota-</option>');

        $('#wilayahKelurahan').find('option').remove()
        $("#wilayahKelurahan").append('<option disabled selected value="0">-Pilih Kabupaten / Kota-</option>');
    }

    function getKecamatanBy(kabupatenkotaId) {
        // body...
        $.ajax({
            type: 'GET',
            url: "{{ URL::to('/') }}/apis/wilayah/{{ $userDataSession->remember_token }}/kecamatan/"+kabupatenkotaId,
            success: function (data) {

                @if($userDataSession->kecamatan_user == '')
                    $("#wilayahKecamatan").append('<option disabled selected value="0">-Pilih Kecamatan-</option>');
                    var kec = 0;
                @else
                    var kec = {!! $userDataSession->kecamatan_user !!}
                @endif

                $.each(data, function (index, value) {
                    // APPEND OR INSERT DATA TO SELECT ELEMENT.
                    if(value.kode_kecamatan === kec) {
                        $('#wilayahKecamatan').append('<option selected value="' + value.kode_kecamatan + '">' + value.nama_kecamatan + '</option>');
                    } else {
                        $('#wilayahKecamatan').append('<option value="' + value.kode_kecamatan + '">' + value.nama_kecamatan + '</option>');
                    }
                });
            }
        });

        $('#wilayahKecamatan').find('option').remove()
        $("#wilayahKecamatan").append('<option disabled selected value="0">-Pilih Kecamatan-</option>');

        $('#wilayahKelurahan').find('option').remove()
        $("#wilayahKelurahan").append('<option disabled selected value="0">-Pilih Kecamatan-</option>');
    }

    function getKelurahanBy(kecamatanId) {
        // body...
        $.ajax({
            type: 'GET',
            url: "{{ URL::to('/') }}/apis/wilayah/{{ $userDataSession->remember_token }}/kelurahan/"+kecamatanId,
            success: function (data) {

                @if($userDataSession->kelurahan_user == '')
                    $("#wilayahKelurahan").append('<option disabled selected value="0">-Pilih Kecamatan-</option>');
                    var kel = 0; 
                @else
                    var kel = {!! $userDataSession->kelurahan_user !!}
                @endif

                $.each(data, function (index, value) {
                    // APPEND OR INSERT DATA TO SELECT ELEMENT.
                    if(value.kode_kelurahan === kel) {
                        $('#wilayahKelurahan').append('<option selected value="' + value.kode_kelurahan + '">' + value.nama_kelurahan + '</option>');
                    } else {
                        $('#wilayahKelurahan').append('<option value="' + value.kode_kelurahan + '">' + value.nama_kelurahan + '</option>');
                    }
                });
            }
        });

        $('#wilayahKelurahan').find('option').remove()
        $("#wilayahKelurahan").append('<option disabled selected value="0">-Pilih Kecamatan-</option>');
    }
</script>

@endsection