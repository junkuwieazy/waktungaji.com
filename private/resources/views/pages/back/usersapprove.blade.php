@extends('templates.back')

@section('backtitle', 'User Terverifikasi')

@section('extra-style')
<!-- DataTables CSS -->
<link href="{{ URL::asset('assets/startbootstrap/vendor/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ URL::asset('assets/startbootstrap/vendor/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="page-wrapper">
    <div style="margin-top: 70px" class="row">

    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> <b>Daftar User Terverifikasi</b> <small>user dapat mengubah datanya setiap saat, dan akan masuk kembali menunggu persetujuan</small>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(session()->has('success_message'))
                        <code style="color: blue"> 
                            {{ session()->get('success_message') }}
                        </code>
                    @endif
                    @if(session()->has('error_message'))
                        <code style="color: red">
                            {{ session()->get('error_message') }}
                        </code>
                    @endif

                    <table width="100%" class="table table-striped table-bordered table-hover" id="ShowDataTablesData">
                        <thead>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>Photo</th>
                                <th>ID Card</th>
                                <th>Email Verify</th>
                                <th>Register With</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>

<div id="photoProfilModal" class="modal fade">  
    <div class="modal-dialog">  
        <div class="modal-content">  
            <div class="modal-header">  
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Foto Profil</h4>  
            </div>

            <div class="modal-body" id="photoProfilDetail"></div>  

            <div class="modal-footer">  
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div>  
        </div>  
    </div>  
</div> 

<div id="idCardModal" class="modal fade">  
    <div class="modal-dialog">  
        <div class="modal-content">  
            <div class="modal-header">  
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Foto Profil</h4>  
            </div>

            <div class="modal-body" id="idCardDetail"></div>  

            <div class="modal-footer">  
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div>  
        </div>  
    </div>  
</div> 

<div id="userModal" class="modal fade">  
    <div class="modal-dialog">  
        <div class="modal-content">  
            <div class="modal-header">  
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Details</h4>  
            </div>

            <div class="modal-body" id="userDataDetail">
                <table class="table table-responsive">
                    <tr>
                        <td>Nama Lengkap</td>
                        <td id="namaLengkap">-nama lengkap-</td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td id="gender">-gender-</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="email">-email-</td>
                    </tr>
                    <tr>
                        <td>No Handphone</td>
                        <td id="noHp">-no handphone-</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td id="alamat">-alamat-</td>
                    </tr>
                    <tr>
                        <td>Tipe Daftar</td>
                        <td id="tipeDaftar">-tipe daftar-</td>
                    </tr>
                </table>
            </div>

            <div id="statusUser" class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div>  
        </div>  
    </div>  
</div>
@stop


@section('extra-script')
<script src="{{ URL::asset('assets/startbootstrap/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/startbootstrap/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/startbootstrap/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<script>
    $(document).ready(function() {

        var userDataWaitStatus = {!!$userWaitStatusData!!}

        $('#ShowDataTablesData').DataTable({
            responsive: true,
            "language": {
                "info": "Menampilkan _START_ user _END_ dari total _TOTAL_",
            },
            "oLanguage": {
                "sLengthMenu": "Tampilkan _MENU_ data",
                "sSearch": "Cari..."
            },
            data: userDataWaitStatus,
            columns: [
                { 
                    data: "full_name_user",
                    render: function(data, type, row, meta){
                        if(type === 'display'){
                            if(row.status_user === 'actived') {
                                data = data + ' <small style="color: green"><b>actived</b></small';
                            } else if(row.status_user === 'blocked') {
                                data = data + ' <small style="color: red"><b>blocked</b></small';
                            } else {
                                data = data + ' <small style="color: red"><b>deteled</b></small';
                            }
                        }

                        return data;
                    }
                },
                { 
                    data: "path_pp_user",
                    render: function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<button id="' + data + '" class="btn btn-success viewPhoto">Lihat</button>';
                        }

                        return data;
                    }
                },
                { 
                    data: "path_idcard_user",
                    render: function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<button id="' + data + '" class="btn btn-info viewIdCard">Lihat</button>';
                        }

                        return data;
                    }
                },
                { data: "email_verify" },
                { data: "signup_with" },
                { 
                    data: "id_user",
                    render: function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<button id="' + data + '" class="btn btn-info viewDataDetail">Detail</button>';
                        }

                        return data;
                    }
                },
            ],
            deferRender:    true,
            scrollY:        200,
            scrollCollapse: true,
            scroller:       true
        });

        $('.viewPhoto').click(function(){
            var photoPath = $(this).attr("id");  
            var photoShow = '<center><img class="img img-responsive" src="{!! URL::to("/")."/storages/"!!}'+photoPath+'"></center>';
            $('#photoProfilDetail').html(photoShow);  
            $('#photoProfilModal').modal("show");
        });

        $('.viewIdCard').click(function(){
            var idCardPath = $(this).attr("id");  
            var idCardShow = '<center><img class="img img-responsive" src="{!! URL::to("/")."/storages/"!!}'+idCardPath+'"></center>';
            $('#idCardDetail').html(idCardShow);  
            $('#idCardModal').modal("show");
        });

        $('.viewDataDetail').click(function(){
            var idUser = $(this).attr("id");

            var findData = SearchJsonBy(parseInt(idUser));
            if (findData) {
                console.log(findData);

                $('#namaLengkap').html(findData.full_name_user); 
                $('#email').html(findData.email_user); 
                $('#gender').html(findData.gender); 
                $('#noHp').html(findData.no_handphone);
                $('#alamat').html(findData.nama_kelurahan +', Kec.'+ findData.nama_kecamatan +', Kab.'+ findData.nama_kabkota +', '+ findData.nama_provinsi);
                $('#tipeDaftar').html(findData.signup_with);

                if(findData.status_user === 'actived') {
                    $('#statusUser').append('<a href="{{ URL::to("/") }}/usersapprove/'+findData.remember_token+'" class="btn btn-danger">Blok Akun</a>');
                } else if(findData.status_user === 'blocked') {
                    $('#statusUser').append('<a href="{{ URL::to("/") }}/usersapprove/'+findData.remember_token+'" class="btn btn-success">Aktifkan Akun</a>');
                }

                $('#userModal').modal("show");
            }            
            
        });

        var SearchJsonBy = function (idUser) {
            var i = null;
            for (i = 0; userDataWaitStatus.length > i; i += 1) {
                if (userDataWaitStatus[i].id_user === idUser) {
                    return userDataWaitStatus[i];
                }
            }

            return null;
        };
    });
</script>
@endsection