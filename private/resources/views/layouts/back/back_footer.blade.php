 <!-- jQuery -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ URL::asset('assets/startbootstrap/vendor/morrisjs/morris.min.js') }}"></script>
    <script src="{{ URL::asset('assets/startbootstrap/data/morris-data.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('assets/startbootstrap/dist/js/sb-admin-2.js') }}"></script>

    @yield('extra-script')

</body>
</html>